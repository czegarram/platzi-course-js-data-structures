class Node {
    constructor (value) {
        this.value = value;
        this.next = null;
    }
}

class MySinglyLinkedList {
    constructor(value) {
        this.head = new Node(value);
        this.tail = this.head;
        this.length = 1;
    }
    append(value) {
        const newNode = new Node(value);
        this.tail.next = newNode;
        this.tail = newNode;
        this.length++;
    }
    prepend(value) {
        const newNode = new Node(value);
        newNode.next = this.head;
        this.head = newNode;
        this.length++;
    }
    insert(index, value) {
        /* let currentNode = this.head;
        let previousNode = null;
        while(index>0 && currentNode !== null) {
            const temp = currentNode;
            previousNode = temp;
            currentNode = currentNode.next;
            index--;
        }

        if (previousNode == null) {
            this.prepend(value);
        } else if (currentNode == null) {
            this.append(value);
        } else {
            let tempNode = currentNode;
            let newNode = new Node(value);
            previousNode.next = newNode;
            newNode.next = tempNode;
            this.length++;
        }*/
        if (index >= this.length) {
            return this.append(value);
        } else if (index === 0) {
            return this.prepend(value);
        }

        const newNode = new Node(value);
        const firstPointer = this.getTheIndex(index - 1);
        const holdingPointer = firstPointer.next;
        firstPointer.next = newNode;
        newNode.next = holdingPointer;
        this.length++;
        return this;
    }

    remove(index, value) {
        // Validando si el index no es mayor a la longitud
        if (index >= this.length) {
            return;
        }

        let firstPointer = this.getTheIndex(index - 1);
        // Si el puntero anterior al index a eliminar no existe
        // quiere decir que estamos en el head
        if (!firstPointer) {
            // si el head tiene un valor en next
            if (this.head.next) {
                // el head.next será el nuevo head
                this.head = this.head.next;
            } else {
                // sino eliminamos el head 
                this.head = null;
            }
        } else {
            // el puntero a eliminar
            let pointerToRemoved = firstPointer.next;
            // eliminanos el index dejando de referenciarlo
            firstPointer.next = pointerToRemoved.next;
            // si el next es nulo, es el nuevo tail
            if (!firstPointer.next) {
                this.tail = firstPointer;
            }
        }
        this.length --;
    }

    getTheIndex(index) {
        if (index < 0 || index >= this.length) return null;

        let counter = 0;
        let currenNode = this.head;
        while (counter !== index) {
            currenNode = currenNode.next;
            counter ++;
        }
        return currenNode;
    }
}

let myLinkedList = new MySinglyLinkedList(1);
myLinkedList.append(2);
myLinkedList.append(3);
myLinkedList.append(4);
myLinkedList.append(5);
console.log(myLinkedList);
myLinkedList.remove(2);
console.log(myLinkedList);