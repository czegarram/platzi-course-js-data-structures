// const array = ["Diego", "Karen", "Oscar"];

class MyArray {
    constructor(){
        this.length = 0;
        this.data = {};
    }
    get(index) {
        return this.data[index];
    }
    push(item) {
        this.data[this.length++] = item;
        return this.data;
    }
    pop() {
        const lastitem = this.data[this.length-1];
        delete this.data[--this.length];
        return lastitem; 
    }
    delete(index) {
        const item = this.data[index];
        this.shiftIndex(index);
        return item;
    }
    shiftIndex(index) {
        for(let i = index; i< this.length - 1; i++) {
            this.data[i] = this.data[i + 1];
        }
        delete this.data[this.length-1];
        this.length--;
    }
    shift() {
        return this.delete(0);
    }
    unshift(item) {
        this.unshiftIndex(0);
        this.data[0] = item;
        return this.data;
    }
    unshiftIndex(index) {
        for(let i = this.length-1; i>= index; i--) {
            this.data[i+1] = this.data[i];
        }
        this.length++;  
    }
}

const myArray = new MyArray();
console.log(myArray.push("Ana"));
console.log(myArray.push("Frank"));
console.log(myArray.push("Oscar"));
console.log(myArray.push("Rocio"));
console.log(myArray.unshift("Cesar"));