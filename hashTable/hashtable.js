class HashTable {
    constructor(size) {
        this.data = new Array(size);
    }
    hashMethod(key) {
        let hash = 0;
        for (let i = 0; i < key.length; i++) {
            hash = (hash + key.charCodeAt(i) * i) % this.data.length;
        }
        return hash;
    }
    set(key, value) {
        const address = this.hashMethod(key);
        if (!this.data[address]) {
            this.data[address] = [];
        }
        this.data[address].push([key, value]);
        return this.data;
    }
    get(key) {
        const address = this.hashMethod(key);
        const currentBucket = this.data[address];
        if (currentBucket) {
            for(let i = 0; i < currentBucket.length; i++) {
                if (currentBucket[i][0] === key) {
                    return currentBucket[i][1];
                }
            }
        }
    }
    remove(key) {
        const address = this.hashMethod(key);
        const currentBucket = this.data[address];
        if (currentBucket) {
            for(let i = 0; i < currentBucket.length; i++) {
                if (currentBucket[i][0] === key) {
                    const value = currentBucket[i][1];
                    currentBucket.splice(i,1);
                    return value;
                }
            }
        }
    }
    keys() {
        let keys = [];
        for(let i = 0; i < this.data.length; i++) {
            const currentBucket = this.data[i];
            if (currentBucket) {
                for (let i = 0; i < currentBucket.length; i++) {
                    keys.push(currentBucket[i][0]);
                }
            }
        }
        return keys;
    }
}

const myHashTable = new HashTable(50);
console.log(myHashTable.set("Diego", 1990));
console.log(myHashTable.set("Mariana", 1998));
console.log(myHashTable.set("Alejandra", 2000));
console.log(myHashTable.get("Mariana"));
console.log(myHashTable.get("Diego"));
console.log(myHashTable.get("Alejandra"));
console.log(myHashTable.get("TBD"));
console.log(myHashTable.keys());
console.log(myHashTable.remove("Diego"));
console.log(myHashTable.remove("Alejandra"));
console.log(myHashTable.remove("Mariana"));
console.log(myHashTable.data);
